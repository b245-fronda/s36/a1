const Task = require("../Models/task.js");

/*Controllers and functions*/

module.exports.getAll = (request, response) => {
	Task.find({})
	// to capture the result of the find method
	.then(result => {
		return response.send(result)
	})
	// .catch method captures the error when the find method is executed.
	.catch(error => {
		return response.send(error)
	})
};

// Add Task on our Database
module.exports.createTask = (request, response) => {
	const input = request.body;
	Task.findOne({name: input.name})
	.then(result => {
		if(result !== null){
			return response.send("The task is already existing!")
		} else {
			let newTask = new Task({
				name: input.name
			});

			newTask.save().then(save => {
				return response.send("The task is successfully added!")
			}).catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error => {
		return response.send(error)
	})
};

// Controller that will delete the document that contains the given object.

module.exports.deleteTask = (request, response) => {
	let idToBeDeleted = request.params.id;

	// findByIdAndRemove - find the document that contains the id and then delete the document
	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
};

// [Activity]
// getTask
module.exports.getTask = (request, response) => {
	let idToBeRetrieved = request.params.id;
	Task.findOne({"_id": idToBeRetrieved})
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
};

// updateTask
module.exports.updateTask = (request, response) => {
	let idToBeUpdated = request.params.id;
	Task.findByIdAndUpdate(idToBeUpdated, {status: "complete"}, {new: true})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
};