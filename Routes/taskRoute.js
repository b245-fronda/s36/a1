const express = require("express")
const router = express.Router();

const taskController = require("../Controllers/taskControllers.js")

/*
	Routes
*/

// Route for getAll
router.get("/get", taskController.getAll);

// Route for createTask
router.post("/addTask", taskController.createTask);

// Route for deleteTask
router.delete("/deleteTask/:id", taskController.deleteTask);

// [Activity]
// Route for getTask
router.get("/:id", taskController.getTask);

// Route for updateTask
router.put("/:id/complete", taskController.updateTask);

module.exports = router;